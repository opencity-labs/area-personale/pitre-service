# Logger configuration
import logging
import os

from dotenv import load_dotenv
load_dotenv()

LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO")
FORMAT = '%(asctime)s [%(levelname)s]: %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger('script_logger')
logger.setLevel(LOG_LEVEL)
